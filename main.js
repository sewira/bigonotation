// O(1) — Constant Time

// 1.Accessing Array Index

const array = [1, 2, 3, 4, 5];

console.log(array[1]); //Mengakses element 2 di index nomor 1

// 2.Pushing and Poping on Stack
array.push(6);
console.log(array, "Menanambah element di index terakhir");
array.pop(-1);
console.log(array, "Mengekuarkan element di index terakhir");

//O(log n) — Logarithmic Time

//1.Traversing an array
const array2 = ["a", "b", "c", "d", "e"];
for (i = 0; i < array2.length; i++) {
  console.log(array2[i]);
}

//2.Comparing two strings
const string1 = "apple";
const string2 = "apple";
console.log(string1 === string2);

//3.Checking for Palindrome
const checkingPalindrome = (str) => str == str.split("").reverse().join("");

console.log(checkingPalindrome("kodok"));
